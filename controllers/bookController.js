
const Book =  require("../models/book");
const {validationResult}  = require('express-validator/check');



 /**
 *@suntiver  Showbook
 */
exports.showBook =  async  (req,res,next) => {
     const  result =   await Book.findAll();

     return res.status(200).json({result})

}


 /**
 *@suntiver  insertBook
 */
exports.insertBook = async (req,res,next) => {

            const  errors = validationResult(req);

            if(!errors.isEmpty()){
                 
                    return  res
                    .status(422).
                    json({message:'Validation failed,entered data is incorrect.',
                        errors:errors.array()
                    })

            }else{
                            const  createBook =  await Book.create({
                            name_book : req.body.name_book,
                            description	: req.body.description,
                            img_book : req.body.img_book,
                            price :req.body.price,
                            comment:req.body.comment,

                     });
                    return res.status(201).json({
                              message:`create a book  success!`,
                              success:true

                    })
            }
      




        } ;

 /**
 *@suntiver  UpdateBook
 */
exports.updateBook = async(req,res,next) =>{

    const  errors = validationResult(req);

    if(!errors.isEmpty()){
                 
            return  res
            .status(422).
            json({message:'Validation failed,entered data is incorrect.',
                errors:errors.array()
            })

    }else{
            const updateBook = await Book.update({
                name_book: req.body.name_book,
                description:req.body.description,
                img_book: req.body.img_book,
                price: req.body.price,
                comment:req.body.comment,
            },
            {
                where:{
                    id_book:req.body.id_book
                }
            })
         

            if(updateBook!=0){
                return res.status(200).json({
                    message:`update the book  id: ${req.body.id_book} success!`,
                    success:true
    
                })

            }else{
                return res.status(400).json({
                    message:`not found the book  id: ${req.body.id_book} !!!`,
                    success:false
    
                })

            }
           

    }

   



}


 /**
 *@suntiver  DeleteBook
 */
 exports.deleteBook = async(req,res,next) =>{
   
    const deleteBook  = await Book.destroy({
        where:{
            id_book:req.params.id
        }
    });
    

    if(deleteBook != 0){
            return res.status(200).json({
                message:`delete the book  id: ${req.params.id} success!`,
                success:true
            })

    }else{
            return res.status(400).json({
                message:`not found  id: ${req.params.id} !!!`,
                success:false
            })

    }
   

 }
