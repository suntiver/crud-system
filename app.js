/*
  Owner:suntiver
  content:การทำ crud เบื้องต้น
  edit:21/08/2020
*/

//Module library
const cors = require("cors");
const express =  require("express");
const bodyParser = require("body-parser");
const {success,error} = require("consola");

//Module in side


//route
const  routeBook  = require("./routes/routeBook");


//Model
const  modelBook  = require('./models/book');

const  app = express();
app.use(cors());
app.use(bodyParser.json());


//variable
const PORT  = 4000;


//For sync  model
//modelBook.sequelize.sync();


//User Route Middleware
app.use('/api/v1/',routeBook);




app.listen(PORT,() =>

        success({message:`Server started on PORT  ${PORT}`,badge:true})  

);