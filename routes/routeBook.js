const  route  = require('express').Router();
const  bookController = require('../controllers/bookController');
const  {body}  = require('express-validator/check');


// route.post("/insert-book",[
//             body('name_book').trim().isLength({min:5}),
//             body('price').toFloat()
// ],async(req,res) =>{

//             await  bookController.insertBook(req.body,res);

// });



route.get("/showBook",bookController.showBook);


route.post("/insert-book",[
    body('name_book').not().isEmpty(),
    body('price').toFloat().not().isEmpty(),
],bookController.insertBook);


route.put("/updateBook",[
    body('name_book').not().isEmpty(),
    body('price').toFloat().not().isEmpty(),


],bookController.updateBook);

route.delete("/deleteBook/:id",bookController.deleteBook);


module.exports = route;